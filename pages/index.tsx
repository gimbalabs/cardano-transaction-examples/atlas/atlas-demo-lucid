import axios from "axios";
import { Lucid } from "lucid-cardano";
import { WalletApi } from 'lucid-cardano';
import { useEffect, useState } from 'react';

const useWalletApi = () => {
  const [walletApi, setWalletApi] = useState<WalletApi>()

  useEffect(() => {
    if (!window.cardano?.nami) return

    window.cardano.nami.enable().then(setWalletApi)
  }, [])

  return walletApi
}

export default function Home() {
  const api =  useWalletApi()

  async function mintLockSecret() {
    const collateralUtxos = await api?.experimental.getCollateral(); 
    const changeAddress   = await api?.getChangeAddress();
    const usedAddresses   = await api?.getUsedAddresses();
    const lucid           = await Lucid.new()

    if (api){
      lucid.selectWallet(api)
    }
    
    const secret = (document.getElementById("secret") as HTMLInputElement).value

    if (collateralUtxos && api){    

      let body = {
        mtgpUsedAddrs: usedAddresses
      , mtgpChangeAddr: changeAddress
      , mtgpCollateral: collateralUtxos[0]
      , mtgpSecret: secret
      , mtgpMintAmount: 100
    }
    
    console.log(usedAddresses)
    console.log("post body: ", body)
    const { data } = await axios.post("http://localhost:8081/txBuild/mintToGuess", body);
    console.log("received: ", data);

          
    const signed = await lucid.fromTx(data.urspTxBodyHex).sign().complete()
    console.log(signed)
    const txHash = await signed.submit()
    console.log("txHash", txHash)
  } else{
    console.log()
  }

  }

/******************************************************/

    async function guess() {
      const collateralUtxos = await api?.experimental.getCollateral(); 
      const changeAddress   = await api?.getChangeAddress();
      const usedAddresses   = await api?.getUsedAddresses();
      const lucid           = await Lucid.new();

      const guess = (document.getElementById("guess") as HTMLInputElement).value

      if (collateralUtxos && api){
        let body = {
          gpUsedAddrs: usedAddresses
        , gpChangeAddr: changeAddress
        , gpCollateral: collateralUtxos[0]
        , gpGuessStr: guess
        }

        console.log(body)

        const { data } = await axios.post("http://localhost:8081/txBuild/guess", body);

        console.log(data)

        if (api){
          lucid.selectWallet(api)
        }

        const signed = await lucid.fromTx(data.urspTxBodyHex).sign().complete()
        console.log("signed: ", signed)
        const txHash = await signed.submit()
        console.log("txHash: ", txHash)
      }
      
    }

  return (
    <div className="container">
      <br />
      <input type="text" id="secret" />
      <button onClick={mintLockSecret} >mint and lock with secret</button>
      <br />
      <br />
      <input type="text" id="guess" />
      <button onClick={guess} >mint name</button>
    </div>
  );
  }
