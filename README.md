# atlas demo lucid
Simple frontend to collect data, post to a backend where transactions are built and a cbor transaction body will be sent back to be signed and submitted. Works only with [nami](https://namiwallet.io/) on preprod testnet. Utilizing [lucid-cardano](https://www.npmjs.com/package/lucid-cardano) to connect wallet, sign and submit transactions.



# Use
- `yarn install`
- `yarn run dev`
- start [atlas backend](https://gitlab.com/gimbalabs/cardano-transaction-examples/atlas/atlas-demo)
